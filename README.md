### Installation

```bash
# from root folder
cd client && npm install
# from root folder
cd server && npm install
# from root folder
npm start
```

### Tests

```bash
# just tests
cd client && npm run test
# coverage report
npm run coverage
```

### Demo

[![Watch the video](https://place-hold.it/664x374&text=DEMO_VIDEO&fontsize=16)](https://gitlab.com/anton.shabanov/btoa/-/blob/main/demo/demo.mov)
