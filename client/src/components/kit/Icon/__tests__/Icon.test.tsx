import { describe, it } from 'vitest';

import { Icon } from '@/components/kit/Icon/Icon';
import { render } from '@/utils/test';

describe('Icon', () => {
  it('renders provided content', () => {
    const { container } = render(<Icon name="flash" />);
    expect(container).toBeInTheDocument();
  });

  it('renders nothing when icon is not in the list', () => {
    // @ts-expect-error just checking the test case
    const { container } = render(<Icon name="unknown-icon" />);
    expect(container.firstChild).toBeEmptyDOMElement();
  });
});
