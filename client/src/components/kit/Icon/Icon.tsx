import { ReactComponent as wind } from './svg/wind.svg';
import { ReactComponent as sun } from './svg/sun.svg';
import { ReactComponent as drop } from './svg/drop.svg';
import { ReactComponent as snowflake } from './svg/snowflake.svg';
import { ReactComponent as flash } from './svg/flash.svg';

const iconsMap = {
  wind,
  sun,
  drop,
  snowflake,
  flash,
};

export type IconName = keyof typeof iconsMap;

type IconProps = React.SVGProps<SVGSVGElement> & {
  name: IconName;
};

export function Icon(props: IconProps) {
  const { name, ...rest } = props;
  const Component = iconsMap[name];
  return Component ? <Component {...rest} /> : null;
}
