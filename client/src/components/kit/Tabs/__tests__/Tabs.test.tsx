import { describe, it, vi } from 'vitest';

import { Tabs } from '@/components/kit/Tabs/Tabs';
import { Tab } from '@/components/kit/Tabs/Tab';
import { faker, render, act, fireEvent } from '@/utils/test';

describe('Tabs', () => {
  it('renders tabs', () => {
    const tabs = [faker.lorem.word(), faker.lorem.word()];
    const initialTab = tabs[0];
    const { getByText } = render(
      <Tabs value={initialTab}>
        {tabs.map((tab) => (
          <Tab key={tab} value={tab}>
            {tab}
          </Tab>
        ))}
      </Tabs>
    );
    tabs.forEach((tab) => {
      expect(getByText(tab)).toBeInTheDocument();
    });
  });

  it('fires onChange event', () => {
    const onChangeSpy = vi.fn();
    const tabs = [faker.lorem.word(), faker.lorem.word()];
    const [newTab, initialTab] = tabs;
    const { getByText } = render(
      <Tabs value={initialTab} onChange={onChangeSpy}>
        {tabs.map((tab) => (
          <Tab key={tab} value={tab}>
            {tab}
          </Tab>
        ))}
      </Tabs>
    );

    act(() => {
      fireEvent.click(getByText(newTab));
    });

    expect(onChangeSpy).toHaveBeenCalledWith(newTab);
  });
});
