import { describe, it, vi } from 'vitest';

import { Tab } from '@/components/kit/Tabs/Tab';
import { faker, render, act, fireEvent } from '@/utils/test';

describe('Tab', () => {
  it('renders tab', () => {
    const value = faker.lorem.word();
    const { getByText } = render(<Tab value={value}>{value}</Tab>);

    expect(getByText(value)).toBeInTheDocument();
  });

  it('fires onClick event', () => {
    const onClickSpy = vi.fn();
    const value = faker.lorem.word();
    const { getByText } = render(
      <Tab value={value} onClick={onClickSpy}>
        {value}
      </Tab>
    );

    act(() => {
      fireEvent.click(getByText(value));
    });

    expect(onClickSpy).toHaveBeenLastCalledWith(value);
  });
});
