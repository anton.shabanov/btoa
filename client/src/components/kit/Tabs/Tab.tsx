import styled from '@emotion/styled';

import { Box } from '@/components/kit/Box/Box';
import { Button } from '@/components/kit/Button/Button';

type TabProps<T> = {
  value?: T;
  active?: boolean;
  children: string;
  onClick?: (value: T | undefined) => void;
};

const Container = styled(Box)`
  & + & {
    margin-left: 24px;
  }
`;

export function Tab<T>(props: TabProps<T>) {
  const { children, value, active = false, onClick } = props;
  return (
    <Container>
      <Button
        variant={active ? 'primary' : undefined}
        onClick={() => onClick?.(value)}
      >
        {children}
      </Button>
    </Container>
  );
}
