import { ReactElement, cloneElement, useState } from 'react';

import { Box } from '@/components/kit/Box/Box';

type TabsProps<T> = {
  value?: T;
  children: Array<ReactElement>;
  onChange?: (value: T) => void;
};

export function Tabs<T>(props: TabsProps<T>) {
  const { children, value, onChange } = props;
  const [active, setActive] = useState<T>(() => {
    if (value) {
      return value;
    }
    return children[0]?.props?.value;
  });
  return (
    <Box display="flex" alignItems="center">
      {children.map((child, index) =>
        cloneElement(child, {
          key: index,
          active: child.props.value === active,
          onClick: (v: T) => {
            setActive(v);
            onChange?.(v);
          },
        })
      )}
    </Box>
  );
}
