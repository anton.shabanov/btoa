import { describe, it } from 'vitest';

import { Box } from '@/components/kit/Box/Box';
import { faker, render } from '@/utils/test';

describe('Box', () => {
  it('renders provided content', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Box>{content}</Box>);
    expect(getByText(content)).toBeInTheDocument();
  });
});
