import styled from '@emotion/styled';
import {
  color,
  ColorProps,
  space,
  SpaceProps,
  layout,
  LayoutProps,
  flexbox,
  FlexboxProps,
  position,
  PositionProps,
  border,
  BorderProps,
  grid,
  GridProps,
  textAlign,
  TextAlignProps,
  lineHeight,
  LineHeightProps,
} from 'styled-system';
import { theme } from '@/components/kit/Theme/config';

export const Box = styled.div<
  ColorProps<typeof theme> &
    SpaceProps<typeof theme> &
    LayoutProps &
    FlexboxProps &
    PositionProps &
    BorderProps &
    GridProps &
    TextAlignProps &
    LineHeightProps
>`
  ${color}
  ${space}
  ${layout}
  ${flexbox}
  ${position}
  ${border}
  ${grid}
  ${textAlign}
  ${lineHeight}
`;
