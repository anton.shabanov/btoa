import { describe, it } from 'vitest';

import { Placeholder } from '@/components/kit/Placeholder/Placeholder';
import { faker, render } from '@/utils/test';

describe('Placeholder', () => {
  it('renders provided content', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Placeholder>{content}</Placeholder>);
    expect(getByText(content)).toBeInTheDocument();
  });
});
