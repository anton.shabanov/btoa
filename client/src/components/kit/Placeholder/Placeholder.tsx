import styled from '@emotion/styled';

import { Box, Text } from '@/components/kit';

type PlaceholderProps = {
  children: string;
};

const DisplayValue = styled(Text)`
  background: ${(props) => props.theme.colors.primaryGradient};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

export function Placeholder(props: PlaceholderProps) {
  const { children } = props;
  return (
    <Box textAlign="center" p={3}>
      <DisplayValue variant="heading1">{children}</DisplayValue>
    </Box>
  );
}
