import { describe, it } from 'vitest';

import { Container } from '@/components/kit/Container/Container';
import { faker, render } from '@/utils/test';

describe('Container', () => {
  it('renders provided content', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Container>{content}</Container>);
    expect(getByText(content)).toBeInTheDocument();
  });
});
