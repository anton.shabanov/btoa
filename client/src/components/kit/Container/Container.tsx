import styled from '@emotion/styled';

import { Box } from '@/components/kit/Box/Box';

export const Container = styled(Box)((props) => ({
  maxWidth: '980px',
  margin: '0 auto',
  padding: props.theme.space[4],
}));
