import { describe, it } from 'vitest';

import { Card } from '@/components/kit/Card/Card';
import { faker, render } from '@/utils/test';

describe('Card', () => {
  it('renders provided content', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Card>{content}</Card>);
    expect(getByText(content)).toBeInTheDocument();
  });

  it('renders title when provided', () => {
    const title = faker.lorem.word();
    const { getByText } = render(<Card title={title} />);
    expect(getByText(title)).toBeInTheDocument();
  });

  it('renders action when provided', () => {
    const action = faker.lorem.word();
    const { getByText } = render(<Card action={action} />);
    expect(getByText(action)).toBeInTheDocument();
  });
});
