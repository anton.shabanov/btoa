import type { ReactNode } from 'react';

import { Box } from '@/components/kit/Box/Box';
import { Text } from '@/components/kit/Text/Text';

type CardProps = {
  title?: string;
  action?: ReactNode;
  children?: ReactNode;
};

export function Card(props: CardProps) {
  const { title, action, children, ...rest } = props;
  const showHeader = Boolean(title || action);
  return (
    <Box bg="background2" borderRadius={20} p={3} {...rest}>
      {showHeader && (
        <Box display="flex" alignItems="center" justifyContent="space-between">
          {title && <Text variant="heading3">{title}</Text>}
          {action}
        </Box>
      )}
      <Box mt={showHeader ? 1 : 0}>{children}</Box>
    </Box>
  );
}
