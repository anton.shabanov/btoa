import { describe, it, vi } from 'vitest';

import { Button } from '@/components/kit/Button/Button';
import { faker, render, fireEvent, act } from '@/utils/test';

describe('Button', () => {
  it('renders button', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Button>{content}</Button>);
    expect(getByText(content)).toBeInTheDocument();
  });

  it('fires onClick event', () => {
    const onClickSpy = vi.fn();
    const content = faker.lorem.word();
    const { getByText } = render(
      <Button onClick={onClickSpy}>{content}</Button>
    );

    act(() => {
      fireEvent.click(getByText(content));
    });

    expect(onClickSpy).toHaveBeenCalled();
  });
});
