import styled from '@emotion/styled';
import { variant as styledVariants } from 'styled-system';
import type { Theme } from '@emotion/react';

type ButtonProps = {
  variant?: 'primary';
};

const variants = ({ theme }: { theme: Theme }) =>
  styledVariants({
    variants: {
      primary: {
        background: theme.colors.primaryGradient,
        color: theme.colors.text100,
      },
    },
  });

export const Button = styled.button<ButtonProps>`
  appearance: none;
  width: 100%;
  border: none;
  font-weight: 600;
  font-size: 14px;
  line-height: 1.21;
  padding: 12px 20px;
  cursor: pointer;
  border-radius: 10px;
  background: transparent;
  color: ${(props) => props.theme.colors.text50};

  ${(props) => variants(props)};
`;
