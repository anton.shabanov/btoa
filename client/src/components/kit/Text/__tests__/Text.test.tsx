import { describe, it } from 'vitest';

import { Text } from '@/components/kit/Text/Text';
import { faker, render } from '@/utils/test';

describe('Text', () => {
  it('renders provided content', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Text>{content}</Text>);
    expect(getByText(content)).toBeInTheDocument();
  });
});
