import styled from '@emotion/styled';
import {
  color,
  ColorProps,
  layout,
  LayoutProps,
  fontSize,
  FontSizeProps,
  space,
  SpaceProps,
  position,
  PositionProps,
  typography,
  TypographyProps,
  flexbox,
  FlexProps,
  variant,
} from 'styled-system';
import { theme } from '@/components/kit/Theme/config';

const variants = {
  heading1: {
    fontWeight: 700,
    fontSize: 36,
    lineHeight: 1.2,
  },
  heading2: {
    fontWeight: 600,
    fontSize: 28,
    lineHeight: 1.21,
  },
  heading3: {
    fontWeight: 600,
    fontSize: 21,
    lineHeight: 1.19,
  },
  heading4: {
    fontWeight: 600,
    fontSize: 18,
    lineHeight: 1.22,
  },
  body: {
    fontSize: 14,
    lineHeight: 1.21,
    fontWeight: 400,
  },
  bodySemibold: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: 1.21,
  },
  caption: {
    fontWeight: 400,
    fontSize: 10,
    lineHeight: 1.2,
  },
};

export const Text = styled.span<
  ColorProps<typeof theme> &
    LayoutProps &
    FontSizeProps &
    SpaceProps<typeof theme> &
    PositionProps &
    TypographyProps &
    FlexProps & {
      variant?: keyof typeof variants;
    }
>`
  ${color}
  ${layout}
  ${fontSize}
  ${space}
  ${position}
  ${flexbox}
  ${typography}
  ${variant({
    variants,
  })}
`;
