import type { ReactNode } from 'react';
import { Global, ThemeProvider, css } from '@emotion/react';
import styled from '@emotion/styled';

import { theme } from './config';

const Root = styled('div')`
  line-height: 1.5;
  font-weight: 400;

  color-scheme: light dark;
  color: ${(props) => props.theme.colors.primary};
  background-color: ${(props) => props.theme.colors.background};

  font-synthesis: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  -webkit-text-size-adjust: 100%;

  min-height: 100vh;
`;

type ThemeProps = {
  children: ReactNode;
};

export function Theme(props: ThemeProps) {
  const { children } = props;
  return (
    <ThemeProvider theme={theme}>
      <Root>
        <Global
          styles={css`
            @import url('https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap');

            * {
              box-sizing: border-box;
            }

            body {
              margin: 0;
              font-family: Inter, system-ui, Avenir, Helvetica, Arial,
                sans-serif;
            }

            h1,
            h2,
            h3,
            p {
              margin: 0;
            }
          `}
        />
        {children}
      </Root>
    </ThemeProvider>
  );
}
