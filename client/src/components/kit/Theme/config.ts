export const theme = {
  fontSizes: [12, 14, 16, 24, 32, 48, 64, 96, 128],
  space: [0, 4, 8, 16, 32, 64, 128, 256],
  colors: {
    background: '#000000',
    background2: '#252525',
    backroundSecondary: '#ffffff1a',
    primary: '#FFFFFF',
    primaryGradient: 'linear-gradient(225deg, #EDBAFF 0%, #A1FFFF 100%)',
    secondary: '#7A7A7A',
    text50: '#7A7A7A',
    text100: '#1A1A1A',
    text200: '#3F3F3F',
  },
};
