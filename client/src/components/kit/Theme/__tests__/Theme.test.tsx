import { describe, it } from 'vitest';
import { render } from '@testing-library/react';

import { Theme } from '@/components/kit/Theme/Theme';
import { faker } from '@/utils/test';

describe('Theme', () => {
  it('renders provided content', () => {
    const content = faker.lorem.word();
    const { getByText } = render(<Theme>{content}</Theme>);
    expect(getByText(content)).toBeInTheDocument();
  });
});
