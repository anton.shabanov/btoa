import styled from '@emotion/styled';

import { Box } from '@/components/kit/Box/Box';
import { uuid } from '@/utils/misc';
import { ChangeEvent } from 'react';

type SwitchProps = {
  value?: boolean;
  onChange?: (value: boolean) => void;
};

const Container = styled(Box)`
  position: relative;

  input[type='checkbox'] {
    height: 0;
    width: 0;
    visibility: hidden;
    position: absolute;
    top: 0;
    left: 0;
  }

  label {
    cursor: pointer;
    text-indent: -9999px;
    width: 36px;
    height: 18px;
    background: ${(props) => props.theme.colors.backroundSecondary};
    display: block;
    border-radius: 100px;
    position: relative;
  }

  label:after {
    content: '';
    position: absolute;
    top: 3px;
    left: 3px;
    width: 12px;
    height: 12px;
    background: #fff;
    border-radius: 50%;
    transition: 0.25s;
  }

  input:checked + label {
    background: ${(props) => props.theme.colors.primaryGradient};
  }

  input:checked + label:after {
    left: calc(100% - 3px);
    transform: translateX(-100%);
    background: ${(props) => props.theme.colors.text100};
  }

  label:active:after {
    width: 16px;
  }
`;

export function Switch(props: SwitchProps) {
  const { value = false, onChange } = props;
  const id = `switch-${uuid()}`;
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChange?.(event.currentTarget.checked);
  };
  return (
    <Container>
      <input
        data-testid="checkbox-input"
        type="checkbox"
        id={id}
        checked={value}
        onChange={handleChange}
      />
      <label data-testid="checkbox-label" htmlFor={id}>
        Toggle
      </label>
    </Container>
  );
}
