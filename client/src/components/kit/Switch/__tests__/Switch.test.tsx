import { describe, it, vi } from 'vitest';

import { Switch } from '@/components/kit/Switch/Switch';
import { render, act, fireEvent } from '@/utils/test';

describe('Switch', () => {
  it('renders unchecked switch by default', () => {
    const { getByTestId } = render(<Switch />);
    const checkbox = getByTestId('checkbox-input');
    const label = getByTestId('checkbox-label');

    expect(checkbox).toBeInTheDocument();
    expect(checkbox).not.toBeChecked();
    expect(label).toBeInTheDocument();
  });

  it('renders checked switch if value provided', () => {
    const { getByTestId } = render(<Switch value={true} />);
    expect(getByTestId('checkbox-input')).toBeChecked();
  });

  it('fires onChange event on click', () => {
    const isChecked = false;
    const onChangeSpy = vi.fn();
    const { getByTestId } = render(
      <Switch value={isChecked} onChange={onChangeSpy} />
    );

    act(() => {
      fireEvent.click(getByTestId('checkbox-label'));
    });

    expect(onChangeSpy).toHaveBeenCalledWith(!isChecked);
  });
});
