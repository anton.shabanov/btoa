import { memo } from 'react';
import styled from '@emotion/styled';

import { Box, Text, Card, Switch } from '@/components/kit';
import { SensorCardIcon } from '@/components/elements/SensorCard/SensorCardIcon';
import type { Sensor } from '@/types/common';

type SensorProps = {
  sensor: Sensor;
  onToggle?: (connect: boolean) => void;
};

const DisplayValue = styled(Text)`
  background: ${(props) => props.theme.colors.primaryGradient};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

function SensorCardBase(props: SensorProps) {
  const { sensor, onToggle } = props;
  const isConnected = sensor.connected;
  return (
    <Card
      data-testid="sensor"
      title={sensor.name}
      action={<Switch value={isConnected} onChange={onToggle} />}
    >
      <Box
        py={2}
        display="flex"
        alignItems="center"
        flexDirection="column"
        textAlign="center"
      >
        <SensorCardIcon sensor={sensor} />
        <DisplayValue variant="heading1" as="h3">
          {sensor.value ? `${sensor.value} ${sensor.unit}` : '-'}
        </DisplayValue>
        <Text display="inline-block" mt={1} variant="body" color="secondary">
          {isConnected ? 'Connected' : 'Disconnected'}
        </Text>
      </Box>
    </Card>
  );
}

export const SensorCard = memo(
  SensorCardBase,
  (prevProps: SensorProps, nextProps: SensorProps) => {
    // if "connected" status changed, invalidate
    if (prevProps.sensor.connected !== nextProps.sensor.connected) return false;

    // if "value" changed, invalidate
    if (prevProps.sensor.value !== nextProps.sensor.value) return false;

    // if sensor changed, invalidate
    return prevProps.sensor.id === nextProps.sensor.id;
  }
);
