import styled from '@emotion/styled';

import { Box, Icon } from '@/components/kit';
import type { IconName } from '@/components/kit';
import type { Sensor } from '@/types/common';

type SensorCardIconProps = {
  sensor: Sensor;
};

const iconByName = new Map<string, IconName>([
  ['Wind', 'wind'],
  ['PM10', 'snowflake'],
  ['PM2.5', 'snowflake'],
  ['Humidity', 'drop'],
  ['Temperature', 'sun'],
  ['Pressure', 'flash'],
]);

const Container = styled(Box)<{ connected: boolean }>`
  background: ${(props) =>
    props.connected
      ? props.theme.colors.primaryGradient
      : props.theme.colors.backroundSecondary};
  color: ${(props) =>
    props.connected
      ? props.theme.colors.text200
      : props.theme.colors.secondary};
`;

export function SensorCardIcon(props: SensorCardIconProps) {
  const { sensor } = props;
  const icon = iconByName.get(sensor.name);
  if (!icon) return null;
  return (
    <Container
      title={`${sensor.name} ${
        sensor.connected ? 'connected' : 'disconnected'
      }`}
      my={2}
      width={60}
      height={60}
      borderRadius={10}
      display="flex"
      alignItems="center"
      justifyContent="center"
      lineHeight={0}
      connected={sensor.connected}
    >
      <Icon name={icon} width={24} height={24} />
    </Container>
  );
}
