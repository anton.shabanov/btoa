import { describe, it } from 'vitest';

import { SensorCard } from '@/components/elements/SensorCard/SensorCard';
import { faker, render } from '@/utils/test';

describe('SensorCard', () => {
  it('renders connected sensor details', () => {
    const sensor = faker.app.sensor({
      name: 'Wind',
      value: '123',
      unit: 'm/s',
      connected: true,
    });
    const { getByText, getByTestId } = render(<SensorCard sensor={sensor} />);

    expect(getByText(sensor.name)).toBeInTheDocument();
    expect(getByText(`${sensor.value} ${sensor.unit}`)).toBeInTheDocument();
    expect(getByText('Connected')).toBeInTheDocument();
    expect(getByTestId('checkbox-input')).toBeChecked();
  });

  it('renders disconnected sensor details', () => {
    const sensor = faker.app.sensor({
      name: 'Wind',
      value: '123',
      unit: 'm/s',
      connected: false,
    });
    const { getByText, getByTestId } = render(<SensorCard sensor={sensor} />);

    expect(getByText(sensor.name)).toBeInTheDocument();
    expect(getByText(`${sensor.value} ${sensor.unit}`)).toBeInTheDocument();
    expect(getByText('Disconnected')).toBeInTheDocument();
    expect(getByTestId('checkbox-input')).not.toBeChecked();
  });

  it('renders value placeholder when it is not provided', () => {
    let value = '';
    const sensor = faker.app.sensor({
      value,
    });
    const { rerender, getByText } = render(<SensorCard sensor={sensor} />);

    expect(getByText('-')).toBeInTheDocument();

    value = '123';
    rerender(<SensorCard sensor={{ ...sensor, value }} />);

    expect(getByText(`${value} ${sensor.unit}`)).toBeInTheDocument();
  });
});
