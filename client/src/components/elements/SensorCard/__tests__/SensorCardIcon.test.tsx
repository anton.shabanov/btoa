import { describe, it } from 'vitest';

import { SensorCardIcon } from '@/components/elements/SensorCard/SensorCardIcon';
import { faker, render } from '@/utils/test';

describe('SensorCardIcon', () => {
  it('renders disconnected sensor icon if present', () => {
    const sensor = faker.app.sensor({
      name: 'Wind',
      connected: false,
    });
    const { container, getByTitle } = render(
      <SensorCardIcon sensor={sensor} />
    );

    expect(container).toBeInTheDocument();
    expect(getByTitle(`${sensor.name} disconnected`)).toBeInTheDocument();
  });

  it('renders connected sensor icon if present', () => {
    const sensor = faker.app.sensor({
      name: 'Wind',
      connected: true,
    });
    const { container, getByTitle } = render(
      <SensorCardIcon sensor={sensor} />
    );

    expect(container).toBeInTheDocument();
    expect(getByTitle(`${sensor.name} connected`)).toBeInTheDocument();
  });

  it('renders nothing if icon was not mapped', () => {
    const sensor = faker.app.sensor({
      name: 'Some random name',
      connected: true,
    });
    const { container } = render(<SensorCardIcon sensor={sensor} />);
    expect(container.firstChild).toBeEmptyDOMElement();
  });
});
