import { describe, it, vi } from 'vitest';

import { SensorList } from '@/components/elements/SensorList/SensorList';
import { faker, render, act, fireEvent } from '@/utils/test';

describe('SensorList', () => {
  it('renders sensors list', () => {
    const sensors = new Array(3).fill(null).map(faker.app.sensor);
    const { getAllByTestId } = render(<SensorList sensors={sensors} />);

    expect(getAllByTestId('sensor').length).toBe(sensors.length);
  });

  it('fires onToggle event', () => {
    const onToggleSpy = vi.fn();
    const sensors = new Array(3).fill(null).map(faker.app.sensor);
    const index = 0;
    const { getAllByTestId } = render(
      <SensorList sensors={sensors} onToggle={onToggleSpy} />
    );

    const toggler = getAllByTestId('checkbox-label').at(index);

    if (toggler) {
      act(() => {
        fireEvent.click(toggler);
      });
    }

    expect(onToggleSpy).toHaveBeenCalledWith(
      sensors[index],
      !sensors[index].connected
    );
  });
});
