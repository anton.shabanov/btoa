import { Box } from '@/components/kit';
import { SensorCard } from '@/components/elements/SensorCard/SensorCard';
import type { Sensor } from '@/types/common';

type SensorListProps = {
  sensors: Array<Sensor>;
  onToggle?: (sensor: Sensor, connect: boolean) => void;
};

export function SensorList(props: SensorListProps) {
  const { sensors, onToggle } = props;
  return (
    <Box
      display="grid"
      gridTemplateColumns={[
        'repeat(1, 1fr)',
        'repeat(2, 1fr)',
        'repeat(3, 1fr)',
      ]}
      gridGap={3}
    >
      {sensors.map((sensor) => (
        <SensorCard
          key={sensor.id}
          sensor={sensor}
          onToggle={(connect) => onToggle?.(sensor, connect)}
        />
      ))}
    </Box>
  );
}
