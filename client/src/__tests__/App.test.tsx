import { describe, it } from 'vitest';

import { App } from '@/App';
import { render } from '@/utils/test';

describe('App', () => {
  it('renders correctly', () => {
    const { getByText } = render(<App />);
    expect(getByText('Dashboard')).toBeInTheDocument();
  });
});
