import {
  useState,
  createContext,
  useContext,
  ReactNode,
  useEffect,
} from 'react';

import type { ApiClient } from '@/types/common';

import { ws } from '@/utils/api';

const ApiContext = createContext<ApiClient<unknown> | undefined>(undefined);
const defaultClient = () => ws(import.meta.env.VITE_WS_ENDPOINT);

type ApiProviderProps<T> = {
  client?: () => ApiClient<T>;
  children: ReactNode;
};

export function useApi<T>() {
  return useContext(ApiContext) as T;
}

export function ApiProvider<T>(props: ApiProviderProps<T>) {
  const { children, client = defaultClient } = props;
  const [apiClient] = useState(() => client());
  useEffect(() => {
    return () => {
      // destroy client here
      apiClient.close();
    };
  }, []);
  return (
    <ApiContext.Provider value={apiClient}>{children}</ApiContext.Provider>
  );
}
