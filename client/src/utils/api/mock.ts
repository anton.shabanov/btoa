import type { ApiClient } from '@/types/common';

export function mock<T>(initialData: Array<T> = []): ApiClient<T> {
  const data: Array<T> = [...initialData];
  let queue: Array<(data: T) => void> = [];

  const send = () => null;

  const close = () => null;

  const subscribe = (
    listener: (data: T) => void,
    includePreviousData = true
  ) => {
    if (includePreviousData) {
      data.forEach((chunk) => listener(chunk));
    }

    // push to queue
    queue.push(listener);

    // return "unsubscribe" callback
    return () => {
      queue = queue.filter((l) => l !== listener);
    };
  };

  return {
    send,
    close,
    subscribe,
  };
}
