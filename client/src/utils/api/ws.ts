import { logger } from '@/utils/logger';
import type { ApiClient } from '@/types/common';

export function ws<T>(host: string): ApiClient<T> {
  const wsClient = new WebSocket(host);
  const data: Array<T> = [];
  let queue: Array<(data: T) => void> = [];

  wsClient.onopen = () => {
    logger.log('[WS] opened');
  };

  wsClient.onerror = (error) => {
    logger.error('[WS] error: ', error);
  };

  wsClient.addEventListener('message', (event) => {
    const eventData = JSON.parse(event.data);
    data.push(eventData);
    queue.forEach((listener) => listener(eventData));
  });

  wsClient.onclose = () => {
    logger.log('[WS] closed');
  };

  const send = (data: Record<string, string>) => {
    wsClient.send(JSON.stringify(data));
  };

  const close = () => {
    if (wsClient.readyState === WebSocket.OPEN) {
      wsClient.close();
    } else {
      // not ideal, better to have sequential retry with the limit of max retries
      setTimeout(() => {
        close();
      }, 100);
    }
  };

  const subscribe = (
    listener: (data: T) => void,
    includePreviousData = true
  ) => {
    if (wsClient.readyState === WebSocket.OPEN && includePreviousData) {
      data.forEach((chunk) => listener(chunk));
    }

    // push to queue
    queue.push(listener);

    // return "unsubscribe" callback
    return () => {
      queue = queue.filter((l) => l !== listener);
    };
  };

  return {
    send,
    close,
    subscribe,
  };
}
