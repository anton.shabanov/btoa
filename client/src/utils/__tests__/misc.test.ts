import { describe, it } from 'vitest';

import { uuid } from '@/utils/misc';

describe('misc', () => {
  describe('uuid', () => {
    it('returns 36 chars length uuid', () => {
      expect(uuid().length).toBe(36);
    });
  });
});
