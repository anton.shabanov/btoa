import type { ReactElement, ReactNode } from 'react';
import { RenderOptions, render } from '@testing-library/react';

import { Theme } from '@/components/kit/Theme/Theme';
import { ApiProvider } from '@/context/Api';
import { mock } from '@/utils/api/mock';
import { faker } from '@/utils/test/faker';
import { ApiClient } from '@/types/common';

type ProvidersProps<T> = {
  children: ReactNode;
  client?: () => ApiClient<T>;
};

function mockedClient<T>() {
  const fakeSensors = new Array(5).fill(null).map(() => faker.app.sensor());
  return mock(fakeSensors) as ApiClient<T>;
}
export function Providers<T>(props: ProvidersProps<T>) {
  const { children, client = mockedClient } = props;
  return (
    <ApiProvider client={client}>
      <Theme>{children}</Theme>
    </ApiProvider>
  );
}

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'queries'>
) => render(ui, { wrapper: Providers, ...options });

// re-export testing lib + custom renderer
export * from '@testing-library/react';
export { customRender as render };
