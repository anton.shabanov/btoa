import { faker as baseFaker } from '@faker-js/faker';

import type { Sensor } from '@/types/common';

const sensor = (overrides: Partial<Sensor> = {}): Sensor => ({
  id: faker.string.uuid(),
  name: faker.animal.cat(),
  connected: faker.datatype.boolean(),
  unit: faker.science.unit().symbol,
  value: faker.string.numeric(),
  ...overrides,
});

// just re-export faker as the lib we rely on in test dummy content + some custom helpers
const faker = {
  ...baseFaker,
  // app is just a "namespace" for our custom fake values
  // very likely not the optimal way, but should be ok for the test task
  app: {
    sensor,
  },
};

export { faker };
