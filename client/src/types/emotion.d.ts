import { theme } from '@/components/kit/Theme/config';

declare module '@emotion/react' {
  export interface Theme {
    colors: (typeof theme)['colors'];
    space: (typeof theme)['space'];
  }
}
