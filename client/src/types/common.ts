export type Sensor = {
  id: string;
  name: string;
  connected: boolean;
  unit: string;
  value: string | number;
};

export type ApiClient<T> = {
  close: () => void;
  subscribe: (
    callback: (data: T) => void,
    includePreviousData: boolean
  ) => () => void;
  send: (data: Record<string, string>) => void;
};
