import { Box, Container, Text, Tabs, Tab, Placeholder } from '@/components/kit';
import { SensorList } from '@/components/elements';
import { useSensors } from '@/hooks';
import { useState } from 'react';

type SensorFilter = 'all' | 'connected';

export function App() {
  const [filter, setFilter] = useState<SensorFilter>('all');
  const { sensors, toggle } = useSensors((items) =>
    filter === 'all' ? items : items.filter((item) => item.connected)
  );
  return (
    <Container>
      <Text as="h1" color="primary" variant="heading2">
        Dashboard
      </Text>
      <Box mt={4}>
        <Tabs onChange={setFilter}>
          <Tab value="all">All Sensort</Tab>
          <Tab value="connected">Connected Sensors</Tab>
        </Tabs>
      </Box>
      <Box mt={4}>
        {!sensors.length && <Placeholder>No sensors found...</Placeholder>}
        <SensorList sensors={sensors} onToggle={toggle} />
      </Box>
    </Container>
  );
}

export default App;
