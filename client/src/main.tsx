import ReactDOM from 'react-dom/client';

import { Theme } from '@/components/kit';
import { ApiProvider } from '@/context/Api';
import { App } from '@/App';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <ApiProvider>
    <Theme>
      <App />
    </Theme>
  </ApiProvider>
);
