import { describe, it, vi } from 'vitest';

import { useSensors } from '@/hooks/useSensors';
import { faker, renderHook, act, Providers } from '@/utils/test';
import { mock } from '@/utils/api/mock';

describe('useSensors', () => {
  it('returns empty result by default', () => {
    const { result } = renderHook(() => useSensors(), { wrapper: Providers });
    expect(Array.isArray(result.current.sensors)).toBeTruthy();
    expect(typeof result.current.toggle).toBe('function');
  });

  it('fills sensors with initial data and reacts on toggle', () => {
    const sensorsMock = new Array(3)
      .fill(null)
      .map(() => faker.app.sensor({ connected: false }));
    const index = 0;
    const sensorMock = sensorsMock[index];
    const sendMock = vi.fn();
    const apiMock = vi.fn().mockImplementation(() => {
      const originalMock = mock(sensorsMock);
      return {
        ...originalMock,
        send: sendMock,
      };
    });
    const { result } = renderHook(() => useSensors(), {
      wrapper: (props) => <Providers {...props} client={apiMock} />,
    });
    expect(result.current.sensors.length).toBe(sensorsMock.length);

    act(() => {
      result.current.toggle(sensorMock, true);
    });

    expect(sendMock).toHaveBeenCalledWith({
      command: 'connect',
      id: sensorMock.id,
    });

    act(() => {
      result.current.toggle(sensorMock, false);
    });

    expect(sendMock).toHaveBeenCalledWith({
      command: 'disconnect',
      id: sensorMock.id,
    });
  });

  it('filters items', () => {
    const sensorsMock = new Array(3)
      .fill(null)
      .map((_, index) => faker.app.sensor({ connected: index === 0 }));
    const connected = sensorsMock.filter((i) => i.connected);

    const { result } = renderHook(
      () => useSensors((items) => items.filter((i) => i.connected)),
      {
        wrapper: (props) => (
          <Providers {...props} client={() => mock(sensorsMock)} />
        ),
      }
    );

    expect(result.current.sensors.length).toBe(connected.length);
  });
});
