import { useState, useEffect } from 'react';

import { useApi } from '@/context/Api';
import type { ApiClient, Sensor } from '@/types/common';

export function useSensors(
  filter: (items: Array<Sensor>) => Array<Sensor> = (i) => i
) {
  const client = useApi<ApiClient<Sensor>>();
  const [sensors, setSensors] = useState<Record<Sensor['id'], Sensor>>({});

  const handleToggle = (sensor: Sensor, connect: boolean) => {
    client.send({ command: connect ? 'connect' : 'disconnect', id: sensor.id });
  };

  useEffect(() => {
    const unsubsribe = client.subscribe((data) => {
      setSensors((oldSensors) => ({
        ...oldSensors,
        [data.id]: data,
      }));
    }, true);

    return () => {
      unsubsribe();
    };
  }, [client]);

  // not ideal honestly, but we can discuss this approach
  return { sensors: filter(Object.values(sensors)), toggle: handleToggle };
}
